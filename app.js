var express = require('express'),
	http = require('http'),
	path = require('path'),
	_ = require('underscore'),
	app = express(),
	server = http.createServer(app),
	fs = require('fs'),
	readline = require('readline'),
	solver = require('./solver');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
	res.sendfile(__dirname + '/solver.html');
});

app.get('/solve', function (req, res) {
	var letters = req.query.letters;
	res.send(solver.solve(letters));
});

server.listen(3000, function() {
	console.log('Express server listening at ' + 3000);
});

