# solver-underscore

This project is a solver for the word-finding game called Sanajahti/Wordament/Scrabble. This version finds all Finnish words in the given letter grid. It uses [kotus](http://kaino.kotus.fi/sanat/nykysuomi/) list of Finnish words as a source of accepted words.

# Running

Type `node app.js`, and go to `localhost:3000` and fill in the grid by typing some letters!

# Screenshots

![Sanajuhta](screenshot.png)
