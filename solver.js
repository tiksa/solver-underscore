var _ = require('underscore'),
	fs = require('fs');

var data = fs.readFileSync('./dict.txt', 'utf8'),
	words = data.split('\n');

var DIM = 4;

var indexWords = function (words) {
	return _.chain(words).map(function (word, index) {
		return { word: word.substring(0, 2), index: index };
	}).uniq(true, function (indexed) {
		return indexed.word;
	}).value();
};

var createGrid = function (letterStr) {
	return _.chain(_.range(DIM)).map(function (x) {
		return _.chain(_.range(DIM)).map(function (y) {
			var letter = letterStr.charAt(y * DIM + x);
			return { x: x, y: y, letter: letter, visit: false };
		}).value();
	}).value();
}

var findUnvisitNeighbors = function (grid, cell) {
	var unvisit = [];
	for (var x = Math.max(0, cell.x - 1); x <= Math.min(cell.x + 1, DIM - 1); x++) {
		for (var y = Math.max(0, cell.y - 1); y <= Math.min(cell.y + 1, DIM - 1); y++) {
			var n = grid[x][y];
			if (!n.visit && !(x === cell.x && y === cell.y))
				unvisit.push(n);
		}
	}
	return unvisit;
}

var filterWords = function (words, str, indexedWords) {
	if (str.length < 2)
		return words;

	if (str.length === 2) {
		var indexObj = _.findWhere(indexedWords, { word: str });
		if (!indexObj)
			return [];

		var startIndex = indexObj.index;
		var endArrIndex = indexedWords.indexOf(indexObj) + 1;
		var endIndex = null;
		if (endArrIndex >= indexedWords.length) {
			endIndex = words.length;
		} else {
			endIndex = indexedWords[endArrIndex].index;
		}


		return words.slice(startIndex, endIndex);
	}

	return _.filter(words, function (word) {
		return word.indexOf(str) > -1;
	});
}

var searchCorrect = function (words, str) {
	if (words[0] === str)
		return str;

	return [];
}

var search = function (grid, cell, str, words) {
	str += cell.letter;
	var possibleWords = filterWords(words, str, indexedWords);
	if (!possibleWords.length)
		return [];

	var foundWords = [];
	var correct = searchCorrect(possibleWords, str);
	if (correct)
		foundWords.push(correct);

	var clonedGrid = cloneGrid(grid);
	clonedGrid[cell.x][cell.y].visit = true;
	var neighbors = findUnvisitNeighbors(clonedGrid, cell);

	var newStr = _.clone(str);
	_.each(neighbors, function (neighbor) {
		foundWords.push(search(clonedGrid, neighbor, newStr, possibleWords));
	});

	return _.flatten(foundWords);
}

var cloneGrid = function (grid) {
	return _.map(grid, function (col) {
		return _.map(col, function (cell) {
			return { x: cell.x, y: cell.y, letter: cell.letter, visit: cell.visit }
		});
	});
};

var solve = function (letterStr) {
	var grid = createGrid(letterStr);

	var found = _.chain(grid)
	.flatten()
	.reduce(function (f, cell) {
		var clonedGrid = cloneGrid(grid);
		f.push(search(clonedGrid, clonedGrid[cell.x][cell.y], '', words));
		return _.flatten(f);
	}, [])
	.uniq()
	.sortBy(function (word) { return -word.length; })
	.value();

	return JSON.stringify(found);
};

var indexedWords = indexWords(words);

module.exports = {
	solve: solve
};
